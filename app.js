'use strict';

var React = require('react-native');
import Home from './app/Screens/home';
import TopList from './app/Screens/topList';
import Users from './app/Screens/users';
import UserVideo from './app/Screens/userVideos';
import Video from './app/Screens/video';
import Web from './app/Screens/web';
import Login from './app/Screens/login';
import UserCenter from './app/Screens/userCenter';
import Search from './app/Screens/search';
import Main from './app/common/Main';


var {
    Navigator,
    AsyncStorage
    } = React;





var App = React.createClass({

    getInitialState:function(){
      return {
          userinfo:{}
      }
    },

    renderScene(router, navigator){
        var Component = null;this._navigator = navigator;
        switch(router.name){
            case "Home":
                Component = Home;
                break;
            case "TopList":
                Component = TopList;
                break;
            case "Users":
                Component = Users;
                break;
            case "UserVideo":
                Component = UserVideo;
                break;
            case "Video":
                Component = Video;
                break;

            case "Web":
                Component = Web;
                break;
            case "Login":
                Component = Login;
                break;
            case "UserCenter":
                Component = UserCenter;
                break;
            case "Search":
                Component = Search;
                break;

            default: //default view
                Component = Home;
        }

        return <Component navigator={navigator} {...router.passProps} app={this}/>
    },

    componentDidMount:function(){

        AsyncStorage.getItem('_user',(err,res)=>{
            //console.log("componentDidMount",res);

            if(err!=null)
                return;
            this.setState({
                userinfo:JSON.parse(res)
            });
        });
    },


    _render:function(){
        return (
            <Navigator
                initialRoute={{name: 'Home'}}
                userinfo={this.state.userinfo}
                renderScene={this.renderScene}
                configureScene={(route) => {
            if (route.sceneConfig) {
              return route.sceneConfig;
            }
            return Navigator.SceneConfigs.FadeAndroid;
          }}
            />
        );


    },

    render() {
        //console.log(this.state.userinfo);
        return this._render();


    }
});

module.exports = App;