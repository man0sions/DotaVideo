var React = require('react-native');

import {styles,userPicDef,BlurView} from '../common/Css';
import {Loading,LoadErr} from '../common/Layout';
import Config from '../common/Config';
import Main from '../common/Main';

var {
    TouchableOpacity,
    Image,
    Text,
    View,
    ScrollView,
    ListView,
    Platform,
    AsyncStorage
    } = React;


import {
    LazyloadListView,
    LazyloadView
} from 'react-native-lazyload';



var RendItem = React.createClass({
    getInitialState:function(){
        return {text: null, status: null};
    },
    _renderBtn: function (row, parent) {
        var sd = parent._isSubscribed(row);
        var params = Main.getSubscribeStatus(1);
        if (sd && sd.status==1)
        {
             params = Main.getSubscribeStatus(0);

        }
        var text = this.state.text!==null ? this.state.text : params.text;
        var status = this.state.status!==null ? this.state.status : params.status;
        return (
            <TouchableOpacity onPress={()=>{parent._subscribe(row,status,this)}}
                              style={[styles.Collection,styles['Collection'+status]]}>
                <Text style={styles['InfoBtn'+status]}>{text}</Text>
            </TouchableOpacity>
        );
    },
    render: function () {
        var row = this.props.row;
        var parent = this.props.parent;
        return (
            <LazyloadView host="ListUsers">
                <View style={[styles.InfoItem,styles.InfoHalf]}>
                        <Image style={[styles.InfoHalf]} source={{uri:row.upic}}>
                            <BlurView blurType="light" style={styles.blur}>
                                <View style={[styles.Infoopact,styles.InfoHalf,styles.itemCenter]}>
                                    <TouchableOpacity onPress={()=>{parent._goRouter(row)}}>
                                        <Image style={[styles.InfouserHalf]} source={require('../images/user-circle.png')}>

                                        <Image style={[styles.InfoAvter]} source={{uri:row.upic}}/>
                                            </Image>
                                    </TouchableOpacity>
                                    <Text style={styles.Infotitle}>{row.uname}</Text>
                                    {this._renderBtn(row, parent)}
                                </View>
                            </BlurView>
                        </Image>
                </View>
            </LazyloadView>

        )
    }
});
/**
 * 发现更多
 * 用法  : <ListUsers />
 * @param
 */



var ListUsers = React.createClass({
    getInitialState: function () {
        var data = Main.initialListData(ListView);
        data.subscribeList = [];
        return data;
    },
    componentDidMount: function () {
        var url = Main.sprintf(Config.users, this.state.page);
        Main.loadData(this, url);
        var userinfo = Main.getUserinfo(this);

        if (userinfo) {

            Main.subscribeList(this,userinfo);

        }


    },
    _renderErrBtn(){
        var url = Main.sprintf(Config.users, this.state.page);


        if (!this.state.loadErr)
            return (
                <LazyloadListView
                    name='ListUsers'
                    dataSource={this.state.dataSource}
                    renderRow={(row) => this._renderItem(row)}
                    onEndReached={()=>{ Main.loadData(this, url)}}
                    contentContainerStyle={[styles.InfoContainer]}
                    scrollRenderAheadDistance={200}
                    renderDistance={100}
                    pageSize={1}
                    initialListSize={10}

                />
            );

        return (
            <TouchableOpacity onPress={() => {Main.loadData(this, url)}}>
                <LoadErr size={25}/>
            </TouchableOpacity>
        );
    },
    render: function () {
        var url = Main.sprintf(Config.users, this.state.page);
        return (
            <View style={[{flex:1}]}>
                {this._renderErrBtn()}
                {this.state.loading ? <Loading size={20} text={'数据加载中'}/> : null}

            </View>

        );
    },
    _goRouter: function (row) {
        row.url = Main.sprintf(Config.userVideo, row.uid, 1, '%s');
        row.title = row.uname;
        Main.goRouter(this, 'UserVideo', row);

    },
    _subscribe: function (row, status,component) {
        var userinfo = Main.getUserinfo(this);

        if (!userinfo)
            return Main.toast('请先登录');

        var status = status || 0;
        var params = Main.getSubscribeStatus(status);


        var method = 'POST';
        var url = Config.host + '?r=subscribe/add';
        delete row.vlist;
        var body = 'user_id=' + userinfo.id + '&vid=' + row.uid + '&content=' + JSON.stringify(row)+'&status='+status;

        Main.fetch({
            url:url,
            method: method,
            body:body,
            token:userinfo.uid
        },(json)=>{
            var data = json.data || {};
            if (data.id) {
                var list = this.state.subscribeList;
                list.push(json);

                this.setState({
                    subscribeList:list
                });
                var params2 = Main.getSubscribeStatus(!status);
                component.setState({
                    text:params2.text,
                    status:params2.status
                });

                Main.toast(params.text+'成功');
            }
            else {

                var msg = params.text+"失败";
                Main.toast(msg);

            }
        });




    },
    _isSubscribed: function (row) {
        var list = this.state.subscribeList;


        for (var j = 0; j < list.length; j++) {
            //console.log(list[i].vid == row.uid,list[i].vid , row.uid,res);

            if (list[j].vid == row.uid)
            {
                var res =  list[j];
                return res;


            }

        }

        return false;
    },

    _renderItem: function (row) {
        return <RendItem row={row} parent={this}/>
    }


});


module.exports = ListUsers;
